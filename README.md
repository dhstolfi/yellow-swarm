Yellow Swarm: LED panels to advise optimal alternative tours to drivers in the city of Malaga
=============================================================================================

Traffic congestion is one of the main concerns of citizens living in big cities. In this article we propose a novel system, called Yellow Swarm, with the aim of improving road traffic at a low cost and easy implementation by using a set of LED panels placed throughout the city in order to spread road traffic over its streets. The system is optimally configured by a new epigenetic algorithm which calculates the time slots for each suggested detour shown in the panels. Yellow Swarm was tested on three scenarios of the city of Malaga, Spain, using the real city layout and actual traffic flows calculated by our flow generator algorithm with data published by the city’s authorities. Our results show shorter average travel times (up to 19 s) and a reduction of greenhouse gas emissions (up to 4.5%) and fuel consumption (up to 1.6%), which have been achieved without increasing the maximum travel times (reduced by 205 s). Yellow Swarm, as a distributed traffic management system in the city, represents a major improvement that benefits drivers on long trips by reducing travel times and preventing traffic jams.

Daniel H. Stolfi and Enrique Alba.
**Yellow Swarm: LED panels to advise optimal alternative tours to drivers in the city of Malaga**
*In: Applied Soft Computing, vol. 109, p. 107566, 2021*
doi> https://doi.org/10.1016/j.asoc.2021.107566

Maps of Malaga used in the article. Requires SUMO Version 0.31.0 (http://www.dlr.de)

Acknowledgments:
----------------

This research was partially funded by the Spanish MINECO and FEDER projects: TIN2016-81766-REDT and TIN2017-88213-R (http://6city.lcc.uma.es/).
